# Tristan Bongolan 44908938
# COMP249 Assignment 2 - Python Web App
# Due Date: 28th April

import bottle
from bottle import Bottle, template, static_file, request, redirect, HTTPError, abort
import model
import session

app = Bottle()

contents = []


@app.route('/')
def index(db):
    session.get_or_create_session(db)
    products = model.product_list(db, None)
    info = {
        'title': "The WT Store",
    }

    return template('index', info, products=products)


@app.route('/product/<id>')
def productPage(db, id):
    # an id needs to be passed for the url and the product get
    # thus an id parameter is needed
    session.get_or_create_session(db)
    url = '/product/'+id
    product = model.product_get(db, id)
    # if the id returns a product that doesnt exist, it redirects to 404 page
    # Video by Diego Mollia as reference
    # https://www.youtube.com/watch?v=Qgi6PTYoswc&list=PLUl9whecZvk648iZ98v-3H3pE4T1--giN
    if product is None:
        abort(404, url)
    else:
        return template('product', id=id, product=product)


@app.route('/cart')
def cart(db):
    session.get_or_create_session(db)
    products = session.get_cart_contents(db)
    return template('cart', products=products)


@app.post('/cart')
def cart_post(db):
    itemid = request.forms['product']
    quantity = request.forms['quantity']
    # pushes the itemid and quantity to db through session
    session.add_to_cart(db, itemid, quantity)
    # post needs to have redirect status 302 and redirects to cart
    # video by Diego Mollia as a reference
    # https://www.youtube.com/watch?v=Qgi6PTYoswc&list=PLUl9whecZvk648iZ98v-3H3pE4T1--giN
    bottle.response.status = 302
    bottle.response.set_header('Location', '/cart')
    return "Redirect"


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


if __name__ == '__main__':

    from bottle.ext import sqlite
    from dbschema import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8010)
