# Tristan Bongolan 44908938
# COMP249 Assignment 2 - Python Web App
# Due Date: 28th April

"""
Code for handling sessions in our web application
"""

from bottle import request, response
import uuid
import json

import model
import dbschema

COOKIE_NAME = 'session'


# UUID validity checker taken from: https://stackoverflow.com/a/33245493
def uuid_check(cookie, version=4):
    try:
        uuid_obj = uuid.UUID(cookie, version=4)
    except:
        return False

    return str(uuid_obj) == cookie


def get_or_create_session(db):
    """Get the current sessionid either from a
    cookie in the current request or by creating a
    new session if none are present.

    If a new session is created, a cookie is set in the response.

    Returns the session key (string)
    """
    cur = db.cursor()
    if uuid_check(request.get_cookie(COOKIE_NAME)):
        sessionid = request.get_cookie(COOKIE_NAME)
        cur.execute("SELECT * FROM sessions WHERE sessionid = ?", (sessionid,))
        id = cur.fetchone()['sessionid']
        return id
    else: #if the cookie is not valid or does not exist
        sid = str(uuid.uuid4())
        response.set_cookie(COOKIE_NAME, sid, path='/')
        cart = []
        sql = "INSERT INTO sessions VALUES (?, ?)"
        cur.execute(sql, (sid, json.dumps(cart),))
        db.commit()
        return sid


def add_to_cart(db, itemid, quantity):
    """Add an item to the shopping cart"""
    cur = db.cursor()
    product = model.product_get(db, itemid)
    cost = product['unit_cost']*float(quantity)
    sessionid = request.get_cookie(COOKIE_NAME)
    item = {
        'id': itemid,
        'quantity': quantity,
        'name': product['name'],
        'cost': cost
    }
    #gets the json "string"
    cur.execute("SELECT * FROM sessions WHERE sessionid = ?", (sessionid,))
    data = cur.fetchone()
    #converts it into a list
    cart = json.loads(data['data'])
    cart.append(item)

    sql = "UPDATE sessions SET data = ? WHERE sessionid = ?"
    cur.execute(sql, (json.dumps(cart), sessionid,))
    db.commit()


def get_cart_contents(db):
    """Return the contents of the shopping cart as
    a list of dictionaries:
    [{'id': <id>, 'quantity': <qty>, 'name': <name>, 'cost': <cost>}, ...]
    """
    sessionid = request.get_cookie(COOKIE_NAME)
    #if the cookie was deleted, it will not direct to error 500
    if sessionid is None:
        return None
        
    cur = db.cursor()
    cur.execute("SELECT * FROM sessions WHERE sessionid = ?", (sessionid,))
    list = json.loads(cur.fetchone()['data'])
    for i in range(len(list)):
        #converts the quantity from string to int
        list[i]['quantity'] = int(list[i]['quantity'])
    return list


